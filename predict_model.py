from SegImages import SegImages
import os
import config


seg = SegImages()

inputtestpath = "D:\\desafio_portal_telemedicina\\chestxray_segmentation\\DATASETS\\CXR8\\kfolds\\test0_input"
outputtestpath = "D:\\desafio_portal_telemedicina\\chestxray_segmentation\\DATASETS\\CXR8\\kfolds\\test0_output"

modelpath = 'D:\\desafio_portal_telemedicina\\chestxray_segmentation\\models\\mobilenet_unet\\checkpoints\\mobilenet_unet_aug0_size224_metric_accuracy_cxr8.h5'

CXR8_OUTPUT_PATH = os.path.sep.join([config.DIRPATH, 'PREDICTIONS', 'CXR8'])

smooth = '0'

orig_dims = (1024,1024)

seg.seg_model_eval(inputtestpath, modelpath, config.LABELS, CXR8_OUTPUT_PATH, False, orig_dims, smooth)
seg.seg_evaluate(inputtestpath, outputtestpath, modelpath, config.LABELS, CXR8_OUTPUT_PATH, config.LABEL_NAMES, smooth)