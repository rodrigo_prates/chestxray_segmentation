from SegImages import SegImages
import os
import config


seg = SegImages()
cxr8_path = "D:\\desafio_portal_telemedicina\\chestxray_segmentation\\DATASETS\\CXR8\\images"
csvpath = "D:\\desafio_portal_telemedicina\\chestxray_segmentation\\seg_cxr8_csv.csv"
labels = config.LABELS
label_names = config.LABEL_NAMES
outputpath_masks = "D:\\desafio_portal_telemedicina\\chestxray_segmentation\\DATASETS\\CXR8\\masks"
outputpath_labels = "D:\\desafio_portal_telemedicina\\chestxray_segmentation\\DATASETS\\CXR8\\labels"
default_background = "FUNDO"
seg.createMasksFromCoords(cxr8_path, csvpath, labels, label_names, outputpath_masks, outputpath_labels, default_background)