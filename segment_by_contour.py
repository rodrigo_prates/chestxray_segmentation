import config
import os
from SegImages import SegImages


seg = SegImages()
imagesPath = "D:\\desafio_portal_telemedicina\\chestxray_segmentation\\DATASETS\\CXR8\\Test"
segsPath = "D:\\desafio_portal_telemedicina\\chestxray_segmentation\\PREDICTIONS\\CXR8\\BLIND_TEST\\mobilenet_unet\\images"
labelDict = {0:(64,0,0), 1: (0, 64, 0)}
outPath = "D:\\desafio_portal_telemedicina\\chestxray_segmentation\\PREDICTIONS\\CXR8\\BLIND_TEST\\mobilenet_unet\\contours"
seg.segmentByContour(imagesPath, segsPath, labelDict, outPath)