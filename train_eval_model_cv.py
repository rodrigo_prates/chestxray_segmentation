import config
import os

if not config.USE_GPU:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from SegImages import SegImages

seg = SegImages()

model = "mobilenet_unet"

custom_loss = 'dice_loss' # dice_loss or categorical_crossentropy

optimizer = 'adadelta'

metric = 'accuracy'

monitor = "val_loss"

aug = '0' # 0 -> False or 1 -> True

database = 'cxr8'

with_smooth = '0' # 0 -> False, 1 -> True

base_name = model + '_' + custom_loss + '_' + optimizer + '_' + metric + '_aug' + aug + '_' + \
            database + '_clw_' + 'smooth_' + with_smooth + '_'

outputname = "result_folds_" + base_name + '.csv'
outputname_pulmao = "pulmao_result_folds_" + base_name + '.csv'
outputname_fundo = "fundo_result_folds_" + base_name + '.csv'
outputname_logs = 'logs_' + base_name + '.csv'

cross_valid_path = config.CXR8_CROSS_VALID_PATH
kfolds = config.KFOLDS
models_path = config.MODELS_PATH
batch_size = config.BATCH_SIZE
labels = config.LABELS
label_names = config.LABEL_NAMES
num_epochs = config.NUM_EPOCHS
models_path = config.MODELS_PATH

output_path = os.path.sep.join([models_path, model])

seg.train_eval_model_cv(model, 
                        kfolds, 
                        cross_valid_path, 
                        models_path,
                        batch_size, 
                        labels, 
                        label_names, 
                        num_epochs, 
                        custom_loss, 
                        optimizer, 
                        metric, 
                        monitor, 
                        aug, 
                        with_smooth,
                        output_path,
                        outputname, 
                        outputname_pulmao, 
                        outputname_fundo, 
                        outputname_logs, 
                        base_name, 
                        database)