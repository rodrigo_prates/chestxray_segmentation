from SegImages import SegImages
import os
import config


seg = SegImages()
CROSS_VALID_PATH = config.CXR8_CROSS_VALID_PATH

if not os.path.exists(CROSS_VALID_PATH):
    os.mkdir(CROSS_VALID_PATH)

inputpath = config.INPUTPATH
labelpath = config.LABELPATH
outputpath = CROSS_VALID_PATH
trainsize = config.TRAINSIZE
kfolds = config.KFOLDS
do_augmentation = False
seg.split_data_cross_validation(inputpath, labelpath, outputpath, trainsize, kfolds, do_augmentation)