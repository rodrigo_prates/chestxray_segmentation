import os


DIRPATH = os.getcwd()
DATASETS_PATH = os.path.sep.join([DIRPATH, 'DATASETS'])
MODELS_PATH = os.path.sep.join([DIRPATH, 'models'])
RESULTS_PATH = os.path.sep.join([DIRPATH, 'RESULTS'])

INPUTPATH = os.path.sep.join([DATASETS_PATH, 'CXR8', 'images'])
LABELPATH = os.path.sep.join([DATASETS_PATH, 'CXR8', 'labels'])
MASKPATH = os.path.sep.join([DATASETS_PATH, 'CXR8', 'masks'])

CXR8_CROSS_VALID_PATH = os.path.sep.join([DATASETS_PATH, 'CXR8', 'kfolds'])

TRAINSIZE = 0.7

KFOLDS = 2

BATCH_SIZE = 4

NUM_LABELS = 2

NUM_EPOCHS = 100

LABELS = {(64,0,0): 0,
          (0, 64, 0): 1}

LABEL_NAMES = ["FUNDO", 
                "PULMAO"]

PATIENCE = 15

USE_GPU = True