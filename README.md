# README #

Relatório contendo:
- instalação; 
- como usar; 
- dataset usado; 
- descrição do método utilizado; 
- resultados visuais para algumas imagens; 
- justificativa e resultados da métrica escolhida e uma estimativa das imagens do dataset que o algoritmo funciona;

## INSTALAÇÂO ##

- Python 3+ (via anaconda por exemplo)
- Criar ambiente virtual: conda create --name tf_gpu tensorflow-gpu
- pip install -r requirements.txt
- fork do keras_segmentation usado nesse projeto: https://bitbucket.org/rodrigo_prates/keras_segmentation/src/master/
  - É preciso dar pull nesse projeto e depois python setup install.

## COMO USAR ##

Foi criada uma classe e scripts para geração dos dados, treinamento e avaliação do modelo. Tudo usa um arquivo de configuração, config.py, que pode ser editado conforme a necessidade.

- create_masks_cxr8.py: a partir das imagens e das coordenadas da segmentação manual(http://www.robots.ox.ac.uk/~vgg/software/via/via.html), gera as labels e mascaras para compor o input do modelo.

- create_kfolds.py: a partir das imagens e das labels gera os inputs para treinamento do modelo por validação cruzada.

- train_eval_model_cv.py: a partir dos folds treina e salva um modelo(.h5) de CNN e avalia o mesmo através das metricas de Jaccard(Iou), Dice, precision, recall etc. Uma pasta RESULTS é criada com algumas tabelas com os valores médios e por classe de cada métrica. A métrica de Jaccard é a referencia de qualidade adotada nesse relatório. O script contempla as opções de augmentation e smooth(pós-processamento), mas por questões de tempo não foram implementadas. Também não implementada a opção de balanceamento entre classes, pois julgou-se não ser necessário nesse caso. Os resultados gerados ao final da execução do scripts são obtidos pela média de todos os folds. São resultados médios e por classe(fundo e pulmao).

- predict_model.py: Avalia o modelo e gera uma predição de mascaras a partir de uma pasta com imagens de teste e segmentações manuais. Essas mascaras preditas podem ser usadas para se gerar os contornos, que são o resultado visual final desse relatório.

- segment_by_contour.py: Cria as linhas de contorno do pulmão a partir das mascaras preditas pelo modelo.

## DATASET ##

Foram usadas 72 imagens da base CXR8(https://github.com/TRKuan/cxr8), fornecida para o desafio. Por questões de tempo, não foi possível segmentar manualmente mais imagens. Também não foi considerado a quantidade de pessoas distintas, nem suas idades, na seleção das imagens para treinamento do modelo.

## MÈTODO UTILIZADO ##

Optou-se por uma rede CNN pré-treinada U-Net com encoder MobilenetV1 para segmentação semantica da região do pulmão. As redes CNN são reconhecidas com o estado-da-arte em muitos problemas de visão computacional. O modelo U-Net é reconhecido como um algoritmo estado-da-arte para segmentação em imagens médicas. A rede Mobilenet foi adicionada para aumentar o numero de camadas sem que isso gere um grande aumento no numero de parâmetros.
Como função custo, foi usada a Dice com a possibilidade de ponderação entre as classes caso necessário. A Dice também é uma recomendação da literatura.
A biblioteca utilizada para treinar o modelo pode ser obtida pelo link: https://github.com/divamgupta/image-segmentation-keras

## RESULTADOS ##

Abaixo alguns resultados visuais alcançados pelo modelo proposto em imagens que não foram usadas no treinamento:

### 00000057_004 ###
![00000057_004](PREDICTIONS/CXR8/BLIND_TEST/mobilenet_unet/contours/predseg_00000057_004.png)

### 00000058_000 ###
![00000058_000](PREDICTIONS/CXR8/BLIND_TEST/mobilenet_unet/contours/predseg_00000058_000.png)

### 00000059_001 ###
![00000059_001](PREDICTIONS/CXR8/BLIND_TEST/mobilenet_unet/contours/predseg_00000059_001.png)

Abaixo a matriz de confusão de um fold de teste:

### Matriz Confusão de um Fold ###
![mat_conf](PREDICTIONS/CXR8/mobilenet_unet/matriz_confusao.png)

Abaixo alguns resultados numéricos:

| metrics | mobilenet_unet |
| :-----: | :-----:        |
| JACCARD | 0.93 +/- 0.026 |
| DICE    | 0.96 +/- 0.015 |
|PRECISION| 0.96 +/- 0.026 |
|RECALL   | 0.97 +/- 0.018 |

## JUSTIFICATIVA ##

A métrica de Jaccard ou IoU (interseção pela união de pixels) é recomendada para medir a eficiência da segmentação de imagens.
Também é da literatura que valores acima de 0.5 de IoU já indicam visualmente uma boa segmentação.
Para todas as imagens testadas, salvo os erros inerentes da segmentação manual, todas as imagens tiveram alto IoU.

## REFERÊNCIAS ##

- Coronavirus disease (COVID-19) detection in Chest X-Ray images using majority voting based classifier ensemble
- CoroNet A deep neural network for detection and diagnosis of COVID-19 from chest x-ray images
- COVID-Net A Tailored Deep Convolutional Neural Network Design for Detection of COVID-19 Cases from chest x-ray images
