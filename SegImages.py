import csv
from csv import writer
import os
import numpy as np
import matplotlib.pyplot as plt
import shutil
import cv2
import math
import PIL
import random
from sklearn.metrics import confusion_matrix, jaccard_score, precision_score, recall_score, accuracy_score, f1_score
from keras.models import Model
from datetime import datetime
from sklearn.model_selection import train_test_split, KFold
from keras_segmentation.models.unet import mobilenet_unet
from keras_segmentation.predict import model_from_checkpoint_path
import config
from imgaug import augmenters as iaa
import imageio
import time
import keras.backend as K
import pandas as pd
import json
from shapely.geometry import Polygon
from sklearn.model_selection import train_test_split, KFold
from keras_segmentation.data_utils.data_loader import *
import gc


class SegImages:

    SEGMODELS = {"mobilenet_unet": mobilenet_unet}

    TRAGETSIZE = {"mobilenet_unet": (224, 224)}

    def __init__(self):
        pass

    def organize_dataset(self, set_type, inputpath, labelpath, outputpath, input_train, label_train, do_augmentation=False): #organize dataset in train/valid/test
        input_set = os.path.sep.join([outputpath, set_type+'_input'])
        label_set = os.path.sep.join([outputpath, set_type+'_output'])

        if os.path.isdir(input_set) and os.listdir(input_set):
            shutil.rmtree(input_set)

        if not os.path.isdir(input_set):
            os.makedirs(input_set)

        if os.path.isdir(label_set) and os.listdir(label_set):
            shutil.rmtree(label_set)

        if not os.path.isdir(label_set):
            os.makedirs(label_set)

        totalfiles = len(input_train)
        counter = 0
        for file in input_train:
           print("Copy input files process: {} %".format(round((100/totalfiles)*counter,2)))
           newfilepath = os.path.sep.join([input_set, file])
           origfilepath = os.path.sep.join([inputpath, file])
           shutil.copy2(origfilepath, newfilepath)
           self.imgredimension(newfilepath, 224, 224)
           counter += 1

        totalfiles = len(label_train)
        counter = 0
        for file in label_train:
           print("Copy input files process: {} %".format(round((100/totalfiles)*counter,2)))
           newfilepath = os.path.sep.join([label_set, file])
           origfilepath = os.path.sep.join([labelpath, file])
           shutil.copy2(origfilepath, newfilepath)
           self.imgredimension(newfilepath, 224, 224)
           counter += 1
        
        if do_augmentation:
            self.color_and_geometric_augmentation(input_set, label_set, config.AUGMENTATION_NUM_TRIES)

    def color_and_geometric_augmentation(self, imagespath, labelpath, num_times=5):
        pass

    def imgredimension(self, imagepath, width, height):
        img = cv2.imread(imagepath)
        dim = (width, height)
        img_resize = cv2.resize(img, dim, interpolation=cv2.INTER_NEAREST)
        cv2.imwrite(imagepath, img_resize)

    def split_data_cross_validation(self, inputpath, labelpath, outputpath, trainsize=0.90, kfolds=4, do_augmentation=False):
        input_list = []
        label_list = []
        for file in sorted(os.listdir(inputpath)):
            input_list.append(file)
        for file in sorted(os.listdir(labelpath)):
            label_list.append(file)

        folds = KFold(n_splits=kfolds, shuffle=True, random_state=1)
        kfold_index=0
        for train_index, test_index in folds.split(input_list):
            input_sub_list = [ input_list[index] for index in train_index ]
            label_sub_list = [ label_list[index] for index in train_index ]
            test_size = 1 - trainsize
            input_train, input_val, label_train, label_val = train_test_split(input_sub_list, label_sub_list, test_size=test_size)
            input_test = [ input_list[index] for index in test_index ]
            label_test = [ label_list[index] for index in test_index ]
            self.organize_dataset('train'+str(kfold_index), inputpath, labelpath, outputpath, input_train, label_train, do_augmentation)
            self.organize_dataset('val'+str(kfold_index), inputpath, labelpath, outputpath, input_val, label_val)
            self.organize_dataset('test'+str(kfold_index), inputpath, labelpath, outputpath, input_test, label_test)
            kfold_index += 1

    def seg_train(self, modelname, label_names, inputtrainpath, inputvalpath, inputtestpath, outputtrainpath, outputvalpath, 
                    outputtestpath, batch_size, output_path, augmentation, metric, database, monitor, num_epochs, optimizer, custom_loss):

        Network = self.SEGMODELS[modelname]
        numlabels = len(label_names)

        num_samples = len([f for f in os.listdir(inputtrainpath) if os.path.isfile(os.path.join(inputtrainpath, f))])
        steps_per_epoch = math.ceil(num_samples/batch_size)
        val_num_samples = len([f for f in os.listdir(inputvalpath) if os.path.isfile(os.path.join(inputvalpath, f))])
        val_steps_per_epoch = math.ceil(val_num_samples/batch_size)
        input_height, input_width = self.TRAGETSIZE[modelname]

        model = Network(n_classes=numlabels, input_height=input_height, input_width=input_width)
        
        checkpoints_path = os.path.sep.join([output_path, 'checkpoints'])
        print('checkpoints_path: {}'.format(checkpoints_path))
        if not os.path.exists(checkpoints_path):
            os.makedirs(checkpoints_path)

        checkpoints_path = os.path.sep.join([checkpoints_path, modelname])
        checkpoints_path = checkpoints_path + '_aug' + augmentation + '_size' + str(input_height) + '_metric_' + metric + '_' + database + '.h5'
        csv_log_filename = modelname + '_aug' + augmentation + '_size' + str(input_height) + '_metric_' + metric + '_' + database + '.csv'

        csv_log_path = os.path.sep.join([output_path, 'logs'])
        if not os.path.exists(csv_log_path):
            os.makedirs(csv_log_path)

        csv_log_path = os.path.sep.join([csv_log_path, csv_log_filename])

        history = model.train(train_images=inputtrainpath, 
                        train_annotations=outputtrainpath, 
                        validate=True, 
                        val_images=inputvalpath, 
                        val_annotations=outputvalpath, 
                        epochs=num_epochs, 
                        batch_size=batch_size, 
                        val_batch_size=batch_size,
                        steps_per_epoch=steps_per_epoch, 
                        val_steps_per_epoch=val_steps_per_epoch, 
                        verify_dataset=False, 
                        auto_resume_checkpoint=False,
                        do_augment=False,
                        augmentation_name=None, 
                        checkpoints_path=checkpoints_path,
                        csv_log_path=csv_log_path, 
                        metric_name=metric, 
                        optimizer_name=optimizer,
                        patience=config.PATIENCE, 
                        monitor=monitor,
                        class_weights=None,
                        custom_loss=None if custom_loss == 'none' else custom_loss)

        return history, model, checkpoints_path

    def evaluate(self, inputfilepath, labelfilepath, model, labels, 
                 conf_matrix, expected_list, recall_list, precision_list, jaccard_list, dice_list, 
                 fp_list, fn_list, tp_list, tn_list, spc_list, smooth):

        output_width = model.output_width
        output_height  = model.output_height
        input_width = model.input_width
        input_height = model.input_height
        n_classes = model.n_classes

        img = cv2.imread(inputfilepath)
        label = cv2.imread(labelfilepath)
        imgwindow_arr = get_image_array(img, input_width, input_height, ordering=IMAGE_ORDERING)
        pr = model.predict(np.array([imgwindow_arr]))[0]
        pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )

        smooth = True if smooth=='1' else False

        if smooth:
            pr = self.smoothByMorph(pr)

        pr = pr.reshape((output_width*output_height))

        gt = get_segmentation_array(label, n_classes, output_width, output_height)
        gt = gt.argmax(-1)

        curr_conf_matrix = confusion_matrix(gt, pr, list(range(0, n_classes)))
        conf_matrix = conf_matrix + curr_conf_matrix
        recall, precision, jaccard, dice = self.performance_scores(gt, pr, list(range(0, n_classes)))

        false_positive, false_negative, true_positive, true_negative, specificity, false_positive_rate, \
        false_negative_rate = self.confusion_matrix_metrics(curr_conf_matrix)

        recall_list.append(recall)
        precision_list.append(precision)
        jaccard_list.append(jaccard)
        dice_list.append(dice)
        fp_list.append(false_positive_rate)
        fn_list.append(false_negative_rate)
        tp_list.append(true_positive)
        tn_list.append(true_negative)
        spc_list.append(specificity)
        expected = np.zeros(n_classes, dtype=np.uint8)
        for v in gt:
            expected[v] = 1
        expected_list.append(expected)

        return conf_matrix, recall_list, precision_list, jaccard_list, dice_list, fp_list, fn_list, tp_list, tn_list, spc_list

    def smoothByMorph(self, prediction):
        pass

    def confusion_matrix_metrics(self, confusion_matrix):
        FP = confusion_matrix.sum(axis=0) - np.diag(confusion_matrix)  
        FN = confusion_matrix.sum(axis=1) - np.diag(confusion_matrix)
        TP = np.diag(confusion_matrix)
        TN = confusion_matrix.sum() - (FP + FN + TP)

        # Sensitivity, hit rate, recall, or true positive rate
        TPR = TP/(TP+FN)
        # Specificity or true negative rate
        TNR = TN/(TN+FP) 
        # Precision or positive predictive value
        PPV = TP/(TP+FP)
        # Negative predictive value
        NPV = TN/(TN+FN)
        # Fall out or false positive rate
        FPR = FP/(FP+TN)
        FPR = [0 if math.isnan(x) else x for x in FPR]
        # False negative rate
        FNR = FN/(TP+FN)
        FNR = [0 if math.isnan(x) else x for x in FNR]
        # False discovery rate
        FDR = FP/(TP+FP)

        # Overall accuracy
        ACC = (TP+TN)/(TP+FP+FN+TN)
        #return FP, FN, TP, TN, TPR, TNR, PPV, NPV, FPR, FNR, FDR, ACC
        return FP, FN, TP, TN, TNR, FPR, FNR

    def performance_scores(self, y_true, y_pred, labels):
        cmat = confusion_matrix(y_true, y_pred, labels)
        recall = recall_score(y_true, y_pred, labels, average=None)
        precision = precision_score(y_true, y_pred, labels, average=None)
        jaccard = jaccard_score(y_true, y_pred, labels, average=None)
        dice = f1_score(y_true, y_pred, labels, average=None)
        return recall, precision, jaccard, dice

    def plotConfMatrix(self, confMatrix, labels, plotpath):
      leftmargin = 1.5 # inches
      rightmargin = 1.5 # inches
      categorysize = 1.5 # inches
      figwidth = leftmargin + rightmargin + (len(labels) * categorysize)
      fig = plt.figure(figsize=(figwidth, figwidth))
      ax = fig.add_subplot(111)
      ax.set_aspect(1)
      fig.subplots_adjust(left=leftmargin/figwidth, right=1-rightmargin/figwidth, top=0.94, bottom=0.1)
      cax = ax.matshow(confMatrix)

      for i in range(confMatrix.shape[0]):
        for j in range(confMatrix.shape[1]):
          c = confMatrix[j,i]
          if c > 0.6:
              color = 'black'
          else:
              color = 'w'
          ax.text(i, j, str(np.round(c,2)),color=color, va='center', ha='center')

      plt.title('Confusion matrix')
      fig.colorbar(cax)
      ax.set_xticks(range(len(labels)))
      ax.set_yticks(range(len(labels)))
      ax.set_xticklabels(labels)
      ax.set_yticklabels(labels)
      ax.set_ylim(len(labels)-0.5, 0-0.5)

      plt.xlabel('Predicted')
      plt.ylabel('True')

      plt.savefig(plotpath)

    def seg_model_eval(self, inputtestpath, modelpath, labels, outputpath, origsize, orig_dims, smooth):
        model = model_from_checkpoint_path(modelpath, True)
        outputpath = os.path.sep.join([outputpath, model.model_name, 'images'])

        if not os.path.exists(outputpath):
            os.makedirs(outputpath)
        
        for imgfile in sorted(os.listdir(inputtestpath)):
            imgfilepath = os.path.sep.join([inputtestpath, imgfile])
            self.paint_prediction(imgfilepath, model, labels, outputpath, origsize, orig_dims, smooth)

    def paint_prediction(self, inputfilepath, model, labels, outputpath, origsize=False, orig_dims=(1024,1024), smooth=False, thres=0.9):

        output_width = model.output_width
        output_height  = model.output_height
        input_width = model.input_width
        input_height = model.input_height
        n_classes = model.n_classes

        img = cv2.imread(inputfilepath)
        height, width = img.shape[:2]

        imgwindow_arr = get_image_array(img, input_width, input_height, ordering=IMAGE_ORDERING)

        pr = model.predict(np.array([imgwindow_arr]))[0]

        pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )

        smooth = True if smooth=='1' else False

        if smooth:
            pr = self.smoothByMorph(pr)

        seg_img = np.zeros((output_height, output_width, 3))

        for c in list(labels.values()):
            color = list(labels.keys())[list(labels.values()).index(c)]
            seg_img[:,:,0] += ( (pr[:,: ] == c )*( color[2] )).astype('uint8')
            seg_img[:,:,1] += ((pr[:,: ] == c )*( color[1] )).astype('uint8')
            seg_img[:,:,2] += ((pr[:,: ] == c )*( color[0] )).astype('uint8')

        if origsize:
            seg_img = cv2.resize(seg_img, orig_dims, interpolation=cv2.INTER_NEAREST)
        else:
            seg_img = cv2.resize(seg_img, (input_width, input_height), interpolation=cv2.INTER_NEAREST)

        cv2.imwrite(os.path.sep.join([ outputpath, 'predseg_' + os.path.basename(inputfilepath) ]), seg_img)

    def segmentByContour(self, imagesPath, segsPath, labelDict, outPath):

        if not os.path.exists(outPath):
            os.makedirs(outPath)

        for segname in os.listdir(segsPath):
            seg = cv2.imread(os.path.sep.join([segsPath, segname]), cv2.IMREAD_GRAYSCALE)
            img = cv2.imread(os.path.sep.join([imagesPath, segname.split('_')[1] + '_' + segname.split('_')[2] ]),cv2.IMREAD_GRAYSCALE)
            img = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)

            edged = cv2.Canny(seg, 10, 50) 
            cv2.waitKey(0)

            contours,_ = cv2.findContours(edged,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

            for i,c in enumerate(contours):
                mask = np.zeros(seg.shape, np.uint8)
                cv2.drawContours(mask,c,-1,255, -1)
                mean,_,_,_ = cv2.mean(seg, mask=mask)
                #print('mean', mean)
                label = 1 if mean > 30.0 else 0
                colour = labelDict.get(label)
                cv2.drawContours(img,c,-1,colour[::-1],4,cv2.LINE_AA)
        
            cv2.imwrite(os.path.sep.join([outPath, segname]), img)

    def seg_evaluate(self, inputtestpath, outputtestpath, modelpath, labels, output_path, label_names, with_smooth):
        print('loading model from: {}'.format(modelpath))
        model = model_from_checkpoint_path(modelpath, True)

        outputpath = os.path.sep.join([output_path, model.model_name])

        if not os.path.exists(outputpath):
            os.makedirs(outputpath)

        conf_matrix = np.zeros((len(labels), len(labels)))
        expected_list = []
        recall_list = []
        precision_list = []
        jaccard_list = []
        dice_list = []
        fp_list = []
        fn_list = []
        tp_list = []
        tn_list = []
        spc_list = []

        for imgfile in sorted(os.listdir(inputtestpath)):
            imgfilepath = os.path.sep.join([inputtestpath, imgfile])
            labelfilepath = os.path.sep.join([outputtestpath, imgfile])
            conf_matrix, recall_list, precision_list, jaccard_list, dice_list, \
            fp_list, fn_list, tp_list, tn_list, spc_list = self.evaluate(imgfilepath, labelfilepath, model, labels, 
                          conf_matrix, expected_list, recall_list, precision_list, jaccard_list, dice_list, 
                          fp_list, fn_list, tp_list, tn_list, spc_list, with_smooth)

        expected_list = np.array( expected_list )

        recall_list = np.array(recall_list)
        recall_mean = np.mean(recall_list, axis=0)
        recall_std = np.std(recall_list, axis=0)
        recall_mean_total = np.mean(recall_mean)
        recall_std_total = np.mean(recall_std)

        precision_list = np.array(precision_list)
        precision_mean = np.mean(precision_list, axis=0)
        precision_std = np.std(precision_list, axis=0)
        precision_mean_total = np.mean(precision_mean)
        precision_std_total = np.mean(precision_std)

        jaccard_list = np.array(jaccard_list)
        jaccard_mean = np.mean(jaccard_list, axis=0)
        jaccard_std = np.std(jaccard_list, axis=0)
        jaccard_mean_total = np.mean(jaccard_mean)
        jaccard_std_total = np.mean(jaccard_std)

        dice_list = np.array(dice_list)
        dice_mean = np.mean(dice_list, axis=0)
        dice_std = np.std(dice_list, axis=0)
        dice_mean_total = np.mean(dice_mean)
        dice_std_total = np.mean(dice_std)

        spc_list = np.array(spc_list)
        spc_mean = np.mean(spc_list, axis=0)
        spc_std = np.std(spc_list, axis=0)
        spc_mean_total = np.mean(spc_mean)
        spc_std_total = np.mean(spc_std)

        fp_list = np.array(fp_list)
        fp_mean = np.mean(fp_list, axis=0)
        fp_std = np.std(fp_list, axis=0)
        fp_mean_total = np.mean(fp_mean)
        fp_std_total = np.mean(fp_std)

        fn_list = np.array(fn_list)
        fn_mean = np.mean(fn_list, axis=0)
        fn_std = np.std(fn_list, axis=0)
        fn_mean_total = np.mean(fn_mean)
        fn_std_total = np.mean(fn_std)

        conf_matrix = conf_matrix / np.max(np.abs(conf_matrix))

        print("Sklearn classes recalls: {} with std {}".format(recall_mean, recall_std))
        print("Sklearn Mean recall: {} with std {}".format(recall_mean_total, recall_std_total))

        print("Sklearn classes precisions: {} with std {}".format(precision_mean, precision_std))
        print("Sklearn Mean precision: {} with std {}".format(precision_mean_total, precision_std_total))

        print("Sklearn classes jaccard: {} with std {}".format(jaccard_mean, jaccard_std))
        print("Sklearn Mean jaccard: {} with std {}".format(jaccard_mean_total, jaccard_std_total))

        print("Sklearn classes dice: {} with std {}".format(dice_mean, dice_std))
        print("Sklearn Mean dice: {} with std {}".format(dice_mean_total, dice_std_total))

        print("Manual classes specificity: {} with std {}".format(spc_mean, spc_std))
        print("Manual Mean specificity: {} with std {}".format(spc_mean_total, spc_std_total))

        print("Manual classes FalsePositive: {} with std {}".format(fp_mean, fp_std))
        print("Manual Mean FalsePositive: {} with std {}".format(fp_mean_total, fp_std_total))

        print("Manual classes FalseNegative: {} with std {}".format(fn_mean, fn_std))
        print("Manual Mean FalseNegative: {} with std {}".format(fn_mean_total, fn_std_total))

        sum_of_rows = conf_matrix.sum(axis=1)
        conf_matrix_normalized = conf_matrix / sum_of_rows[:, np.newaxis]
        conf_matrix_normalized = np.nan_to_num(conf_matrix_normalized, 0)
        print("confusion matrix", conf_matrix_normalized)
        np.savetxt(os.path.sep.join([ outputpath, 'matriz_confusao.csv' ]), conf_matrix_normalized, delimiter=",")
        plotpath = os.path.sep.join([ outputpath, 'matriz_confusao.png' ])
        self.plotConfMatrix(conf_matrix_normalized, label_names, plotpath)

        return jaccard_mean_total, jaccard_std_total, jaccard_mean, jaccard_std, \
            dice_mean_total, dice_std_total, dice_mean, dice_std, \
            precision_mean_total, precision_std_total, precision_mean, precision_std, \
            recall_mean_total, recall_std_total, recall_mean, recall_std, \
            spc_mean_total, spc_std_total, spc_mean, spc_std, \
            fp_mean_total, fp_std_total, fp_mean, fp_std, \
            fn_mean_total, fn_std_total, fn_mean, fn_std

    def train_eval_model_cv(self, modelname, kfolds, cross_valid_path, models_path, batch_size, labels, label_names, num_epochs, 
                        custom_loss, optimizer, metric, monitor, augmentation, with_smooth, output_path, outputname, 
                        outputname_pulmao, outputname_fundo, outputname_logs, base_name, database):

        val_losses = []
        jaccard_means = []
        jaccard_stds = []
        jaccard_classes_means = []
        jaccard_classes_stds = []
        dice_means = []
        dice_stds = []
        dice_classes_means = []
        dice_classes_stds = []
        precision_means = []
        precision_stds = []
        precision_classes_means = []
        precision_classes_stds = []
        recall_means = []
        recall_stds = []
        recall_classes_means = []
        recall_classes_stds = []
        specificity_means = []
        specificity_stds = []
        specificity_classes_means = []
        specificity_classes_stds = []
        false_positive_means = []
        false_positive_stds = []
        false_positive_classes_means = []
        false_positive_classes_stds = []
        false_negative_means = []
        false_negative_stds = []
        false_negative_classes_means = []
        false_negative_classes_stds = []    
    
        for idx in range(0, kfolds):
            print('Fold {} of {}'.format(idx+1, kfolds))

            inputtrainpath = os.path.sep.join( [cross_valid_path, 'train'+str(idx)+'_input'] )
            inputvalpath = os.path.sep.join( [cross_valid_path, 'val'+str(idx)+'_input'] )
            inputtestpath = os.path.sep.join( [cross_valid_path, 'test'+str(idx)+'_input'] )

            outputtrainpath = os.path.sep.join( [cross_valid_path, 'train'+str(idx)+'_output'] )
            outputvalpath = os.path.sep.join( [cross_valid_path, 'val'+str(idx)+'_output'] )
            outputtestpath = os.path.sep.join( [cross_valid_path, 'test'+str(idx)+'_output'] )

            if 'val_mean_io_u' in monitor and idx > 0:
                monitor = monitor + '_' + str(idx)

            history, model, checkpoints_path = self.seg_train(modelname, label_names, inputtrainpath, inputvalpath, inputtestpath, outputtrainpath, outputvalpath,
            outputtestpath, batch_size, output_path, augmentation, metric, database, monitor, num_epochs, optimizer, custom_loss)

            if 'val_mean_io_u' in monitor:
                monitor = 'val_mean_io_u'

            val_losses.append(history.history['val_loss'])
            print('val_losses', val_losses)

            jaccard_mean_total, jaccard_std_total, jaccard_mean, jaccard_std, \
            dice_mean_total, dice_std_total, dice_mean, dice_std, \
            precision_mean_total, precision_std_total, precision_mean, precision_std, \
            recall_mean_total, recall_std_total, recall_mean, recall_std, \
            spc_mean_total, spc_std_total, spc_mean, spc_std, \
            fp_mean_total, fp_std_total, fp_mean, fp_std, \
            fn_mean_total, fn_std_total, fn_mean, fn_std = self.seg_evaluate(inputtestpath, outputtestpath, checkpoints_path, labels, output_path, label_names, with_smooth)

            jaccard_means.append(jaccard_mean_total)
            jaccard_stds.append(jaccard_std_total)

            jaccard_classes_means.append(jaccard_mean)
            jaccard_classes_stds.append(jaccard_std)

            dice_means.append(dice_mean_total)
            dice_stds.append(dice_std_total)

            dice_classes_means.append(dice_mean)
            dice_classes_stds.append(dice_std)

            precision_means.append(precision_mean_total)
            precision_stds.append(precision_std_total)

            precision_classes_means.append(precision_mean)
            precision_classes_stds.append(precision_std)

            recall_means.append(recall_mean_total)
            recall_stds.append(recall_std_total)

            recall_classes_means.append(recall_mean)
            recall_classes_stds.append(recall_std)

            specificity_means.append(spc_mean_total)
            specificity_stds.append(spc_std_total)

            specificity_classes_means.append(spc_mean)
            specificity_classes_stds.append(spc_std)

            false_positive_means.append(fp_mean_total)
            false_positive_stds.append(fp_std_total)

            false_positive_classes_means.append(fp_mean)
            false_positive_classes_stds.append(fp_std)

            false_negative_means.append(fn_mean_total)
            false_negative_stds.append(fn_std_total)

            false_negative_classes_means.append(fn_mean)
            false_negative_classes_stds.append(fn_std)

            K.clear_session()
            del model
            gc.collect()

        val_losses = self.format_matrix(val_losses)
        print('val_losses', val_losses)

        val_losses_std = np.std(np.array(val_losses), axis=0)
        val_losses = np.mean(np.array(val_losses), axis=0)

        print('val_losses_mean', val_losses)
        print('val_losses_std', val_losses_std)

        csv_df = pd.DataFrame()
        csv_df['metrics'] = ['JACCARD', 'DICE', 'PRECISION', 'RECALL', 'SPC', 'FP', 'FN']

        csv_df_pulmao = pd.DataFrame()
        csv_df_pulmao['metrics'] = ['JACCARD', 'DICE', 'PRECISION', 'RECALL', 'SPC', 'FP', 'FN']

        csv_df_fundo = pd.DataFrame()
        csv_df_fundo['metrics'] = ['JACCARD', 'DICE', 'PRECISION', 'RECALL', 'SPC', 'FP', 'FN']

        idx_pulmao = label_names.index("PULMAO")
        idx_fundo = label_names.index("FUNDO")

        csv_df[modelname] = [str(np.mean(jaccard_means)) + '+/-' + str(np.mean(jaccard_stds)),
                            str(np.mean(dice_means)) + '+/-' + str(np.mean(dice_stds)), 
                            str(np.mean(precision_means)) + '+/-' + str(np.mean(precision_stds)),
                            str(np.mean(recall_means)) + '+/-' + str(np.mean(recall_stds)),
                            str(np.mean(specificity_means)) + '+/-' + str(np.mean(specificity_stds)),
                            str(np.mean(false_positive_means)) + '+/-' + str(np.mean(false_positive_stds)),
                            str(np.mean(false_negative_means)) + '+/-' + str(np.mean(false_negative_stds))]

        csv_df_pulmao[modelname] = [str(np.mean(jaccard_classes_means, axis=0)[idx_pulmao]) + '+/-' + str(np.mean(jaccard_classes_stds, axis=0)[idx_pulmao]),
                                str(np.mean(dice_classes_means, axis=0)[idx_pulmao]) + '+/-' + str(np.mean(dice_classes_stds, axis=0)[idx_pulmao]), 
                                str(np.mean(precision_classes_means, axis=0)[idx_pulmao]) + '+/-' + str(np.mean(precision_classes_stds, axis=0)[idx_pulmao]),
                                str(np.mean(recall_classes_means, axis=0)[idx_pulmao]) + '+/-' + str(np.mean(recall_classes_stds, axis=0)[idx_pulmao]),
                                str(np.mean(specificity_classes_means, axis=0)[idx_pulmao]) + '+/-' + str(np.mean(specificity_classes_stds, axis=0)[idx_pulmao]),
                                str(np.mean(false_positive_classes_means, axis=0)[idx_pulmao]) + '+/-' + str(np.mean(false_positive_classes_stds, axis=0)[idx_pulmao]),
                                str(np.mean(false_negative_classes_means, axis=0)[idx_pulmao]) + '+/-' + str(np.mean(false_negative_classes_stds, axis=0)[idx_pulmao])]

        csv_df_fundo[modelname] = [str(np.mean(jaccard_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(jaccard_classes_stds, axis=0)[idx_fundo]),
                                str(np.mean(dice_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(dice_classes_stds, axis=0)[idx_fundo]), 
                                str(np.mean(precision_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(precision_classes_stds, axis=0)[idx_fundo]),
                                str(np.mean(recall_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(recall_classes_stds, axis=0)[idx_fundo]),
                                str(np.mean(specificity_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(specificity_classes_stds, axis=0)[idx_fundo]),
                                str(np.mean(false_positive_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(false_positive_classes_stds, axis=0)[idx_fundo]),
                                str(np.mean(false_negative_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(false_negative_classes_stds, axis=0)[idx_fundo])]

        results_path = config.RESULTS_PATH
        if not os.path.exists(results_path):
            os.makedirs(results_path)

        csv_df.to_csv( os.path.sep.join([results_path, outputname]), index=False )
        csv_df_pulmao.to_csv( os.path.sep.join([results_path, outputname_pulmao]), index=False )
        csv_df_fundo.to_csv( os.path.sep.join([results_path, outputname_fundo]), index=False )

        plotfilepath = os.path.sep.join([results_path, 'plot_training_history_' + base_name + '.png'])
        self.plottraininghistory(val_losses, val_losses_std, modelname, plotfilepath)

    def format_matrix(self, matrix):
        max_cols = len(max(matrix, key=len))
        extended_matrix = []
        for row in matrix:
            row = list(row)
            extra_cols = max_cols-len(row)
            row.extend( [row[-1]]*extra_cols )
            extended_matrix.append(row)
        return extended_matrix

    def plottraininghistory(self, history, error, title, plotfilepath):
        error_minus = history-error
        error_plus = history+error
        plt.style.use('ggplot')
        plt.figure()
        epochs = len(history)
        x = list(range(epochs))
        plt.plot(x, list(history), label=title, color='b')
        plt.fill_between(x, list(error_minus), list(error_plus), alpha=0.5, edgecolor='b', facecolor='b')
        plt.title('Validation Loss')
        plt.xlabel('Epoch #')
        plt.ylabel('Loss')
        plt.legend(loc='upper right')
        plt.savefig(plotfilepath)

    def getCoordsFromCsv(self, csvfile):
        x_arr_list = []
        y_arr_list = []
        file_name_list = []
        mask_type_list = []
        with open(csvfile) as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=',')
            for row in csv_reader:
                if int(row['region_count']) > 0:
                    file_name_list.append(row['filename'])
                    x_arr_list.append(json.loads(row['region_shape_attributes'])['all_points_x'])
                    y_arr_list.append(json.loads(row['region_shape_attributes'])['all_points_y'])
                    mask_dict = json.loads(row['region_attributes'])
                    if not mask_dict.items():
                        print('empty label in: ', row['filename'])
                    mask_type_list.append([k for k, v in mask_dict.items() if v][0])

        return file_name_list, x_arr_list, y_arr_list, mask_type_list

    def createMasksFromCoords(self, imagesfolder, csvpath, labels, label_names, outputpath_masks, 
                                outputpath_labels, default_background):
        file_name_list, X_arr_list, Y_arr_list, mask_type_list = self.getCoordsFromCsv(csvpath)
        unique_filenames = np.unique( np.array(file_name_list) )
        print(file_name_list)
        for filename in unique_filenames:
            print('mask for file: {}'.format(filename))
            imagepath = os.path.sep.join([imagesfolder, filename])
            img = cv2.imread(imagepath)
            height, width = img.shape[:2]
            mask = np.zeros(shape=[height, width, 3], dtype=np.uint8)
            label = np.zeros(shape=[height, width, 3], dtype=np.uint8)
            label_idx = label_names.index(default_background)
            color = list(labels.keys())[label_idx]
            label_code = list(labels.values())[label_idx]
            mask[:] = color[::-1]
            label[:] = label_code
            file_idxs = np.where(np.array(file_name_list) == filename)[0]
            for idx in file_idxs:
                X_arr = X_arr_list[idx]
                Y_arr = Y_arr_list[idx]
                mask_type = mask_type_list[idx]
                print('mask type: {}'.format(mask_type))
                label_idx = label_names.index(mask_type)
                color = list(labels.keys())[label_idx]
                label_code = list(labels.values())[label_idx]
                coords = list(zip(X_arr, Y_arr))
                polygon = Polygon(coords)
                int_coords = lambda x: np.array(x).round().astype(np.int32)
                exterior = [int_coords(polygon.exterior.coords)]
                cv2.fillPoly(label, exterior, color=(label_code,label_code,label_code))
                cv2.fillPoly(mask, exterior, color=color[::-1])
            outputmaskpath = os.path.sep.join([outputpath_masks, filename])
            outputlabelpath = os.path.sep.join([outputpath_labels, filename])
            cv2.imwrite(outputmaskpath, mask)
            cv2.imwrite(outputlabelpath, label)
